// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Aguilar_TLD_TestEditorTarget : TargetRules
{
	public Aguilar_TLD_TestEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("Aguilar_TLD_Test");
	}
}
