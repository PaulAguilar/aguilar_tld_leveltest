// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Aguilar_TLD_TestTarget : TargetRules
{
	public Aguilar_TLD_TestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Aguilar_TLD_Test");
	}
}
