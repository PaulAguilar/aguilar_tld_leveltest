// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Aguilar_TLD_TestGameMode.h"
#include "Aguilar_TLD_TestCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAguilar_TLD_TestGameMode::AAguilar_TLD_TestGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
