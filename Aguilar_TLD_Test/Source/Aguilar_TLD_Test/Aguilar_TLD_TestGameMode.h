// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Aguilar_TLD_TestGameMode.generated.h"

UCLASS(minimalapi)
class AAguilar_TLD_TestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAguilar_TLD_TestGameMode();
};



